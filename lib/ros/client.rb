require 'xmlrpc/client' # @see https://rubygems.org/gems/rubysl-xmlrpc

require 'ros/error'

module ROS
  ##
  # @see http://wiki.ros.org/ROS/Master_Slave_APIs
  class Client
    ##
    # @return [String]
    attr_reader :id

    ##
    # @return [XMLRPC::Client]
    attr_reader :xmlrpc

    ##
    # @param  [#to_s] master_url
    # @return [void]
    def initialize(master_url = nil, **args)
      @id = args[:id] || '/rosrb'
      @master_url = (master_url || ENV['ROS_MASTER_URI']).to_s
      @xmlrpc = XMLRPC::Client.new2(@master_url, args[:proxy], args[:timeout])
    end

    ##
    # @param  [Symbol] operator
    # @param  [Array] operands
    # @return [void]
    def call(operator, *operands)
      code, msg, result = @xmlrpc.call(operator, id, *operands)
      case code
        when -1 then raise Error::ServiceError, msg.to_s
        when 0  then raise Error::ServiceFailure, msg.to_s
        when 1  then result
      end
    end

    ##
    # @see http://wiki.ros.org/ROS/Master_API
    module MasterAPI
      ##
      # @param  [String] service
      # @param  [String] service_api
      # @param  [String] caller_api
      # @return [void]
      def register_service(service, service_api, caller_api)
        call('registerService', service.to_s, service_api.to_s, caller_api.to_s); nil
      end

      ##
      # @param  [String] service
      # @param  [String] service_api
      # @return [Integer]
      def unregister_service(service, service_api)
        call('unregisterService', service.to_s, service_api.to_s)
      end

      ##
      # @param  [String] topic
      # @param  [String] topic_type
      # @param  [String] caller_api
      # @return [Array(String)]
      def register_subscriber(topic, topic_type, caller_api)
        call('registerSubscriber', topic.to_s, topic_type.to_s, caller_api.to_s)
      end

      ##
      # @param  [String] topic
      # @param  [String] caller_api
      # @return [Integer]
      def unregister_subscriber(topic, caller_api)
        call('unregisterSubscriber', topic.to_s, caller_api.to_s)
      end

      ##
      # @param  [String] topic
      # @param  [String] topic_type
      # @param  [String] caller_api
      # @return [String]
      def register_publisher(topic, topic_type, caller_api)
        call('registerPublisher', topic.to_s, topic_type.to_s, caller_api.to_s)
      end

      ##
      # @param  [String] topic
      # @param  [String] caller_api
      # @return [Integer]
      def unregister_publisher(topic, caller_api)
        call('unregisterPublisher', topic.to_s, caller_api.to_s)
      end

      ##
      # @param  [String] node_name
      # @return [String]
      def lookup_node(node_name)
        call('lookupNode', node_name.to_s)
      end

      ##
      # @param  [String] subgraph
      # @return [Array(Array(String, String))]
      def get_published_topics(subgraph = '')
        call('getPublishedTopics', subgraph.to_s)
      end

      ##
      # @return [Array(Array(String, String))]
      def get_topic_types
        call('getTopicTypes')
      end

      ##
      # @return [Array(Array, Array, Array)]
      def get_system_state
        call('getSystemState')
      end

      ##
      # @return [String]
      def get_uri
        call('getUri')
      end

      ##
      # @param  [String] service
      # @return [String] the service URL
      def lookup_service(service)
        call('lookupService', service.to_s)
      end
    end # MasterAPI

    ##
    # @see http://wiki.ros.org/ROS/Parameter%20Server%20API
    module ParameterAPI
      ##
      # @param  [String] key
      # @return [void]
      def delete_param(key)
        call('deleteParam', key.to_s); nil
      end

      ##
      # @param  [String] key
      # @param  [Object] value
      # @return [void]
      def set_param(key, value)
        call('setParam', key.to_s, value.to_s); nil
      end

      ##
      # @param  [String] key
      # @return [Object]
      def get_param(key)
        call('getParam', key.to_s)
      end

      ##
      # @param  [String] key
      # @return [String]
      def search_param(key)
        call('searchParam', key.to_s)
      end

      ##
      # @param  [String] caller_api
      # @param  [String] key
      # @return [Object]
      def subscribe_param(caller_api, key)
        call('subscribeParam', caller_api.to_s, key.to_s)
      end

      ##
      # @param  [String] caller_api
      # @param  [String] key
      # @return [Integer]
      def unsubscribe_param(caller_api, key)
        call('unsubscribeParam', caller_api.to_s, key.to_s)
      end

      ##
      # @param  [String] key
      # @return [Boolean]
      def has_param(key)
        call('hasParam', key.to_s)
      end

      ##
      # @return [Array(String)]
      def get_param_names
        call('getParamNames')
      end
    end # ParameterAPI

    ##
    # @see http://wiki.ros.org/ROS/Slave_API
    module SlaveAPI
      ##
      # @return [Array(Array, Array, Array)]
      def get_bus_stats
        call('getBusStats')
      end

      ##
      # @return [Array]
      def get_bus_info
        call('getBusInfo')
      end

      ##
      # @return [String]
      def get_master_uri
        call('getMasterUri')
      end

      ##
      # @param  [String] msg
      # @return [void]
      def shutdown(msg = '')
        call('shutdown', msg.to_s); nil
      end

      ##
      # @return [Integer]
      def get_pid
        call('getPid')
      end

      ##
      # @return [Array(Array(String, String))]
      def get_subscriptions
        call('getSubscriptions')
      end

      ##
      # @return [Array(Array(String, String))]
      def get_publications
        call('getPublications')
      end

      ##
      # @param  [String] parameter_key
      # @param  [Object] parameter_value
      # @return [void]
      def param_update(parameter_key, parameter_value)
        call('paramUpdate', parameter_key.to_s, parameter_value); nil
      end

      ##
      # @param  [String] topic
      # @param  [Array(String)] publishers
      # @return [void]
      def publisher_update(topic, publishers)
        call('publisherUpdate', topic.to_s, publishers.to_a); nil
      end

      ##
      # @param  [String] topic
      # @param  [Array] protocols
      # @return [Array]
      def request_topic(topic, protocols)
        call('requestTopic', topic.to_s, protocols.to_a)
      end
    end # SlaveAPI

    include MasterAPI, ParameterAPI, SlaveAPI
  end # Client
end # ROS
