module ROS
  ##
  # Base class for ROS exceptions.
  #
  # @see http://wiki.ros.org/roscpp/Overview/Exceptions
  # @see http://wiki.ros.org/rospy/Overview/Exceptions
  class Error < StandardError
    class ServiceError < Error; end
    class ServiceFailure < Error; end

    class InternalError < Error; end
    class InvalidName < Error; end
    class InvalidNodeName < InvalidName; end
    class SerializationError < Error; end
  end # Error
end # ROS
