require 'ros/type'

module ROS
  ##
  # @see http://wiki.ros.org/Messages
  class MessageType
    ##
    # @param  [Pathname, #to_s] pathname
    # @return [MessageType]
    def self.load(pathname)
      self.parse(File.read(pathname.to_s))
    end

    ##
    # @param  [String, #to_s] input
    # @return [MessageType]
    def self.parse(input)
      raise "not implemented yet" # TODO
    end

    ##
    # @param  [#map] field_types
    # @return [void]
    def initialize(field_types, *values)
      @template = Type.compile(field_types)
      @values = values
    end

    def pack
      @values.pack(@template)
    end
  end # MessageType
end # ROS
