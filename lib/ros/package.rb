require 'pathname'

module ROS
  ##
  # @see http://wiki.ros.org/Packages
  class Package
    ##
    # The package name.
    #
    # @return [String] the package name
    attr_reader :name

    ##
    # @param  [String, #to_s] name the package name
    # @return [void]
    def initialize(name)
      @name = name.to_s
    end
  end # Package
end # ROS
