module ROS
  ##
  # @see http://wiki.ros.org/Clock
  # @see http://wiki.ros.org/roscpp/Overview/Time
  class Time
    ##
    # Returns the current time.
    #
    # @return [Time]
    def self.now
      t = ::Time.now
      self.new(t.tv_sec, t.tv_nsec)
    end

    ##
    # @return [Integer]
    attr_reader :sec

    ##
    # @return [Integer]
    attr_reader :nsec

    ##
    # @param  [#to_i] sec
    # @param  [#to_i] nsec
    def initialize(sec, nsec = 0)
      @sec, @nsec = Integer(sec), Integer(nsec)
    end

    ##
    # @return [Array(Integer, Integer}]
    def to_a
      [@sec, @nsec]
    end

    ##
    # @return [Float]
    def to_f
      @sec + @nsec / 1E6
    end

    ##
    # @return [Integer]
    def to_i
      @sec
    end
  end # Time
end # ROS
