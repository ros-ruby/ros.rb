module ROS
  ##
  # @see http://wiki.ros.org/msg
  module Type
    # TODO: http://answers.ros.org/question/215165/what-is-the-endianness-of-ros-message-fields/
    PRIMITIVES = {
      :bool     => 'C',
      :int8     => 'c',
      :uint8    => 'C',
      :int16    => 's<',
      :uint16   => 'S<',
      :int32    => 'l<',
      :uint32   => 'L<',
      :int64    => 'q<',
      :uint64   => 'Q<',
      :float32  => 'f<',
      :float64  => 'd<',
      :string   => nil,  # TODO
      :time     => nil,  # TODO
      :duration => nil,  # TODO
      :char     => 'C',  # @deprecated
      :byte     => 'c',  # @deprecated
    }.freeze

    ##
    # @param  [#map] types the type specification
    # @param  [Array] values the input values
    # @return [String] a binary output string
    def self.pack(types, *values)
      values.pack(self.compile(types))
    end

    ##
    # @param  [#map] types the type specification
    # @param  [String] input the binary input string
    # @return [Array] the output values
    def self.unpack(types, input)
      input.unpack(self.compile(types))
    end

  protected

    ##
    # @param  [#map] types
    # @return [String] a `#pack`/`#unpack` specification
    def self.compile(types)
      directives = types.map do |type|
        PRIMITIVES[type = type.to_sym] ||
          (raise TypeError, "unknown ROS primitive type: #{type}")
      end
      directives.join
    end
  end # Type
end # ROS
