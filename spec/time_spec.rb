require 'ros/time'

RSpec.describe ROS::Time do
  it "returns the correct current time" do
    expect(ROS::Time.now.to_i).to eq(::Time.now.to_i)
  end

  it "can be converted to an Array" do
    expect(ROS::Time.new(42, 123456).to_a).to eq([42, 123456])
  end

  it "can be converted to a Float" do
    expect(ROS::Time.new(42, 123456).to_f).to eq(42.123456)
  end

  it "can be converted to an Integer" do
    expect(ROS::Time.new(42, 123456).to_i).to eq(42)
  end
end
