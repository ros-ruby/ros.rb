require 'ros/type'

RSpec.describe ROS::Type do
  def pack(*args);   ROS::Type.pack(*args);   end
  def unpack(*args); ROS::Type.unpack(*args); end

  it "packs int8 fields" do
    expect(pack(%i(int8), 0x7F)).to eq("\x7F".b)
  end

  it "packs uint8 fields" do
    expect(pack(%i(uint8), 0xFF)).to eq("\xFF".b)
  end

  # TODO

  it "unpacks int8 fields" do
    expect(unpack(%i(int8), "\x7F".b)).to eq([0x7F])
  end

  it "unpacks uint8 fields" do
    expect(unpack(%i(uint8), "\xFF".b)).to eq([0xFF])
  end

  # TODO
end
